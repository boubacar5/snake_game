var canvas = document.getElementById('canvas');
var objet = canvas.getContext('2d'); 

// dimension grid
var grid = { 
    width : 12,
    height : 6,
}

var espace_x = 17; // colenne
var espace_y = 9; // ligne

var snake_x = 4;// Snake dans position x
var snake_y = 10;// Snake dans position y

var pomme_x = 11; // pomme position
var pomme_y = 10; // pomme position

var direction_x = 0; // Snake direction X
var direction_y = 0; // Snake direction Y 

var score = 0; // score

// les fonction du jeu
function jeu(){ 
    // les limites du serpent et game over
  if(snake_x <= -1 || snake_y <=-1 || snake_x >= 18 || snake_y >= 18){
    alert(`Perdu!!!`); 
    location.reload() // recommence
   }

    Snake(); //Snake
    pomme(); //pomme
    Pomme_random(); 
    ChangePosition(); //Change Position
    setTimeout(jeu , 230  ) // vitesse serpent
}

// context serpent 
function Snake(){
    objet.fillStyle = 'white';
    objet.fillRect(snake_x * espace_x ,snake_y * espace_y, grid.width, grid.height)  
}

// context pomme
function pomme(){
    objet.fillStyle = 'red';
    objet.fillRect(pomme_x * espace_x ,pomme_y * espace_y , grid.width, grid.height)
}

// movement serpent
function ChangePosition(){
    snake_x = snake_x + direction_x;
    snake_y = snake_y + direction_y;
}

// collision serpent et pomme
function Pomme_random(){ 
  if(pomme_x ===  snake_x && pomme_y === snake_y){
        pomme_x = Math.floor(Math.random() *  espace_x);
        pomme_y = Math.floor(Math.random() *  espace_y);
    }
}

// direction serpent avec clavier
document.body.addEventListener('keydown' ,  (en)=>{
    switch (en.keyCode) {
          // en haut
        case 38:
            direction_y =  -1;
            direction_x =  0; 
            break;
    
         // en bas
        case 40:
            direction_y =  1;
            direction_x = 0; 
            break;

          //gauche
        case 37:
            direction_y =  0;
            direction_x =  -1; 
            break;
    
         //droit
        case 39:
            direction_y =  0;
            direction_x =  1; 
            break;
    }
})
jeu(); 